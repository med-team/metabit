#!/usr/bin/env Rscript

read.abundance.table <- function(filename) {
  d <- read.table(filename, row.names=1, sep='\t', as.is=TRUE)
  if( row.names(d)[1] == "ID" ) {
    names(d) <- d[1,]
    if(ncol(d) < 2){
      d <- subset(d, row(d)>=2)
    } else {
      d <- d[-1,]
    }
  }
  taxa.names <- rownames(d)

  if(ncol(d) < 2){
    d[,1] <- as.numeric(d[,1])
  } else {
    d <- apply(d, 2, as.numeric)
  }

  rownames(d) <- taxa.names
  return(d)
}

doHeatmap <- function(d, out.pdf=NULL, graph.title=NULL, taxon.title="Taxon",
                      color_low='white', color_mid='red', color_high='black',
                      midpoint=50) {
  require(ggplot2, quietly=T)
  require(reshape2, quietly=T)

  # re-order lines and columns (default clustering):
  taxa.order    <- hclust(dist(d))$order
  if( ncol(d)>2 ) {
    samples.order <- hclust(dist(t(d)))$order
    heatmap.data  <- d[taxa.order, samples.order]
  } else { 
    # samples.order <- seq_len(ncol(d))
    heatmap.data  <- d[taxa.order,,drop=FALSE]   # drop=FALSE to keep data.frame
  }


  heatmap.data <- melt(t(heatmap.data), varnames=c("sample", taxon.title),
                       value.name="abundance")
  heatmap.data[[taxon.title]] <- gsub("_", " ", heatmap.data[[taxon.title]])
  out.heatmap.plot <- ggplot(heatmap.data, aes_string(x="sample", y=taxon.title)) +
                      geom_tile(aes(fill=abundance)) +
                      scale_fill_gradient2(low=color_low,
                                           mid=color_mid,
                                           high=color_high,
                                           midpoint=midpoint) +
                      theme(axis.text.x=element_text(angle=90, size=8,
                                                     hjust=1, vjust=.5),
                            axis.text.y=element_text(size=8)) +
                      ggtitle(graph.title)
  if( !is.null(out.pdf) ) pdf(out.pdf)
  print(out.heatmap.plot)
  if( !is.null(out.pdf) ) invisible(dev.off())
}

if( !interactive() ) {
  library(optparse)
  option_list <- list(
    make_option("--color_low", default='white', help="color of low values in heatmap"),
    make_option("--color_mid", default='red',   help="color of mid values in heatmap"),
    make_option("--color_high",default='black', help="color of high values in heatmap"),
    make_option("--midpoint",  default = 50, type='integer', 
                help="midpoint position for the color scale in heatmap"),
    make_option("--title", help="title of the plot"),
    make_option("--taxon_title", default="Taxon", help="title of the Y-axis [Taxon]")
    )
  Parser <- OptionParser(prog = "doHeatmap.R", option_list=option_list)
  cl <- parse_args(Parser, positional_arguments=2)

  d <- read.abundance.table(cl$args[1])
  
  doHeatmap(d, cl$args[2],
            graph.title=cl$options$title,
            taxon.title=cl$options$taxon_title,
            color_low=cl$options$color_low,
            color_mid=cl$options$color_mid,
            color_high=cl$options$color_high,
            midpoint=cl$options$midpoint)
}

