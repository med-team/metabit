#!/usr/bin/env Rscript

if( !interactive() )
  rm(list=ls(all=T))

### FUNCTIONS
my.write.table <- function(Table, file, sep="\t", quote=F) {
  Table <- cbind(rownames(Table), Table)
  names(Table)[1] <- ""
  write.table(Table, file, col.names=T, row.names=F, quote=quote, sep=sep)
}

gglike_colors <- function(n, light=65, chroma=100, start=15) {
  hues = seq(start, 360+start, length=n+1)
  hcl(h=hues, l=light, c=chroma)[seq_len(n)]
}

doPCoA <- function(d,  # column names will be used for the distance
                   out.pcoa="pcoa",
                   out.pcoa.pdf = NULL,
                   out.pcoa.tsv = NULL,
                   main=NULL,
                   distance="bray",
                   x.axis=1,
                   y.axis=2,
                   inverse.x=1,
                   inverse.y=1,
                   color.in=rainbow(nrow(d)),
                   color.out=rainbow(nrow(d)),
                   symbol=21,
                   cex=1,
                   text.col='black', #text.col arg in legend
                   legend.text=rownames(d),
                   is.interactive=interactive())
{
  out.pcoa.pdf <- ifelse(is.null(out.pcoa.pdf),
                         paste0(out.pcoa,".pdf"),
                         out.pcoa.pdf)
  out.pcoa.tsv <- ifelse(is.null(out.pcoa.tsv),
                         paste0(out.pcoa,".tsv"),
                         out.pcoa.tsv)


  require(ape, quietly=T)
  suppressPackageStartupMessages(require(vegan, quietly=T))
  distances <- vegdist(d, method=distance)

  pcoa.result <- pcoa(distances)
  x.pcoa.r <- pcoa.result$vectors[,x.axis] * inverse.x
  y.pcoa.r <- pcoa.result$vectors[,y.axis] * inverse.y

  my.write.table(as.matrix(distances), file=out.pcoa.tsv)

  psymbol <- paste(color.in, color.out, symbol)
  dup.symbol <- duplicated(psymbol)
  
  if( length(legend.text) == nrow(d)) {
    legend.text <- paste(legend.text[!dup.symbol],
                         ifelse(unique(psymbol) %in% psymbol[dup.symbol], "...", ""))
  } else if( (excess.legend <- length(legend.text) - sum(!dup.symbol)) <0 ) {
    warning(sprintf(paste("Length of legend (%s)",
                          "smaller than the number of unique plotting symbols (%s)"),
                    length(legend.text), sum(!dup.symbol)),
            call.=F)
  } else if ( excess.legend > 0) {
    # removing unused legend
    legend.text <- head(legend.text, sum(!dup.symbol))
  }

  data.psymbol <- t(data.frame(strsplit(psymbol[!dup.symbol], " ")))
  legend.in <- data.psymbol[,1]
  legend.out <- data.psymbol[,2]
  legend.pch <- as.numeric(data.psymbol[,3])

  h <- 1.1 * (max(y.pcoa.r) - min(y.pcoa.r))

  legend.cols <- ceiling(length(legend.text)/30)
  # 0.08 is approximately the width of a character in inches, whatever par("cin") is
  # 0.8 is the cex
  # 1.2 for adding the space between columns
  mai.right <- 1.2 * legend.cols * 0.8 * max( 0.08*nchar(legend.text) )

  dev_width <- 7 + mai.right

  if ( !is.interactive ) {
    pdf(out.pcoa.pdf, useDingbats=FALSE, width=dev_width)
  } else {
    dev.new(width=dev_width)
  }
  
  oldmai <- par("mai")[1:3]
  par(mai=c(oldmai, mai.right), pty='s')

  # rev() because the first points are usually more important
  # (drawn at last: on top)
  plot(rev(x.pcoa.r), rev(y.pcoa.r),
       pch=rev(symbol),
       cex=rev(cex),
       cex.axis=0.9,
       font.lab=2,
       font.axis=2,
       xlab=paste0("Dimension ", x.axis),
       ylab=paste0("Dimension ", y.axis),
       col=rev(color.out),
       bg=rev(color.in))
  title(main)
  
  legend(x = par("usr")[2], y = par("usr")[4], legend = legend.text,
         pch=legend.pch, col=legend.out, pt.bg=legend.in, text.col=text.col,
         bty='n', xpd=TRUE, xjust=0, cex=.8, pt.cex=1.5, ncol=legend.cols)

#  number_of_samples = 37  ## uncomment and replace XX with number of the samples or libs analyzed.
#  point.labels <- row.names(pcoa.result$vectors)[1:number_of_samples]
#  text(x=x.pcoa.r[1:number_of_samples], y=y.pcoa.r[1:number_of_samples], labels=point.labels, font=1, cex=0.2, pos=3, col="black")
  
  if (!is.interactive) invisible(dev.off())
}


if( !interactive() ) {
  library(optparse, quietly=T)

  ### Command-line parsing
  option_list <- list(
    make_option(
          c("-m", "--makefile"),
          help=paste("R file defining plotting parameters:",
                     "\t\tcol (external color of the points. Default=rainbow)",
                     "\t\tbg (internal color of the points. Default=rainbow)",
                     "\t\tpch (style of the point, Default=21)",
                     "\t\tlegend (legend text. Default to column names from columns with a unique style)",
                     "\t\ttitle (Default: basename option)", 
                     "\t\tany argument allowed in the plot() function can be given",
                     sep='\n')
          ),
    make_option("--basename", default="pcoa",
                help=paste("default basename for the output files",
                           "(a .tsv and a .pdf will be created). ",
                           "Alternatively you can use --pdf and --tsv.")),
    make_option("--pdf", default=NULL, help="output pdf file"),
    make_option("--tsv", default=NULL, help="output tsv file"),
    make_option(c("-b", "--binary"), action="store_true",
                help="convert all abundances in presence/absence"),
    make_option(c("-d", "--distance"), default='bray',
                help="distance used to compute the distance matrix [bray]"),
    make_option(c("-x", "--x-axis"), default=1, type='integer',
                help="which dimension of the PCoA is on the X axis."),
    make_option(c("-y", "--y-axis"), default=2, type='integer',
                help="which dimension of the PCoA is on the Y axis."),
    make_option("--inv-x", action="store_true", default=FALSE,
                help="invert the X axis"),
    make_option("--inv-y", action="store_true", default=FALSE,
                help="invert the Y axis")
  )

  Parser <- OptionParser(prog = "doPcoa.R",
                         description=paste("Compute the distance matrix between ",
                                      "columns of the input table, then compute",
                                      "the Principal Coordinates Analysis (PCoA)",
                                      "and produce the corresponding plot."),
                         option_list=option_list)
  cl <- parse_args(Parser, positional_arguments=1)

 
  if (cl$args == "-") cl$args <- "stdin"
  d <- read.table(cl$args, row.names=1, sep='\t', as.is=TRUE)

  if (ncol(d) <= 2 ) {
    msg <- c("Error: PCoA cannot be done with less than 3 samples",
             paste0("(Input file: ", cl$args[1], ")"))
    write(msg, file=stderr())
    if( !interactive() ) {
      pdf(cl$options$pdf)
      plot.new()
      text(0, c(1, .95), msg, adj=0, col='red')
      invisible(dev.off())
      write(msg, file=cl$options$tsv)
      quit(save="no", status=0)
    }
  }

  if( row.names(d)[1] == "ID" ) {
    names(d) <- d[1,]
    d <- d[-1,]
  }
  td <- t(d)
  sample.names <- rownames(td)
  td <- apply(td, 2, as.numeric)
  rownames(td) <- sample.names

  # Load all variables defined in the makefile to a list called params
  default.params <- list(legend=names(d),
                         col=rainbow(ncol(d)),
                         bg=rainbow(ncol(d)),
                         pch=21,
                         cex=1.5,
                         text.col='black',
                         title=NULL)

  if (is.null(cl$options$makefile)) {
    params <- default.params
  } else {
    makefile.env <- new.env()
    assign("sample.names", sample.names, envir=makefile.env)
    sys.source(cl$options$makefile, envir=makefile.env)
    params <- as.list(makefile.env)
    params <- c(params, default.params)
    extra.params <- params[ !(names(params) %in% names(default.params)) ] 
  }

  doPCoA(td,
         out.pcoa = cl$options$basename,
         out.pcoa.pdf = cl$options$pdf,
         out.pcoa.tsv = cl$options$tsv,
         main = params$title,
         distance = cl$options$distance,
         x.axis = cl$options[["x-axis"]],
         y.axis = cl$options[["y-axis"]],
         inverse.x= ifelse(cl$options[["inv-x"]], -1, 1),
         inverse.y= ifelse(cl$options[["inv-y"]], -1, 1),
         color.in=params$bg,
         color.out=params$col,
         symbol=as.numeric(params$pch),
         cex=as.numeric(params$cex),
         text.col=params$text.col,
         legend.text=params$legend,
         is.interactive=interactive())
}
