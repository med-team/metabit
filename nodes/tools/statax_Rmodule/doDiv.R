#!/usr/bin/env Rscript

read.abundance.table <- function(filename) {
  d <- read.table(filename, row.names=1, sep='\t', as.is=TRUE)
  if( row.names(d)[1] == "ID" ) {
    names(d) <- d[1,]
    if(ncol(d) < 2){
      d <- subset(d, row(d)>=2)
    } else {
      d <- d[-1,]
    }
  }
  taxa.names <- rownames(d)

  if(ncol(d) < 2){
    d[,1] <- as.numeric(d[,1])
  } else {
    d <- apply(d, 2, as.numeric)
  }
  rownames(d) <- taxa.names
  return(d)
}

my.write.table <- function(Table, file, sep="\t", quote=F) {
  Table <- cbind(rownames(Table), Table)
  names(Table)[1] <- ""
  write.table(Table, file, col.names=T, row.names=F, quote=quote, sep=sep)
}

split.table <- function(Table, rm.strains=TRUE,
                        default.taxlevels=c("k", "p", "c", "o", "f", "g", "s")) {

    if( rm.strains ) {
      Table <- subset(Table, !grepl('\\|t__', row.names(Table)))
      # Table <- Table[!grepl('\\|t__', row.names(Table)),]
    }
    sample_names <- names(Table)
    taxa_names   <- strsplit(row.names(Table), '|', fixed=T)
    taxlevel   <- sapply(taxa_names, function(e) default.taxlevels[length(e)])
    lasttaxon  <- sapply(taxa_names, tail, 1) 
      ## Data1 <- sapply(default.taxlevels, function(x) Table[taxlevel==x,],
      ##              simplify=F, USE.NAMES=T)

    Data <- sapply(default.taxlevels, function(x) subset(Table, taxlevel==x),
                   simplify=F, USE.NAMES=T)
    Data
    #warning: you should check that the row.names do not contain duplicated taxa. 
}

filter.table <- function(Data) {
  # remove 100% unclassified
  if( "unclassified" %in% rownames(Data$k) ) {
    unclassified <- (Data$k["unclassified",] == 100)
    write(c("The following samples contain 100% unclassified:",
            colnames(Data$k)[unclassified]),
          file=stderr())
    Data <- sapply(Data, `[`, TRUE, !unclassified)
    Data
  } else{
    write("No samples are 100% unclassified", file=stderr())
    Data
  }
}

doDiv <- function(Data, index="shannon") {
  suppressPackageStartupMessages(require(vegan, quietly=T))
  diversities <- sapply(Data, diversity, index=index, MARGIN=2,
                        USE.NAMES=T)
  return(diversities)
}


if( !interactive() ) {
  library(optparse)
  option_list <- list(
    make_option("--index", default='shannon', #choices = c('bray', dist.methods),
                help="diversity index (allowed by vegan::diversity) [shannon]"),
    make_option("--taxlevels", default='kpcofgs',
                help=paste("taxonomical levels to create one table per level.",
                           "(kingdom phylum class order family genus species (strain))",
                           "'kpcofgst' to include strains (metaphlan2)")),
    make_option("--strains", default=TRUE, action='store_false', dest="rmstrains",
                help=paste("keep strains from data (metaphlan2).",
                           "For a metaphlan2 table, either --strains or ",
                           "--taxlevels 'kpcofgst' must be used"))
  )

  Parser <- OptionParser(prog = "doDiv.R", option_list=option_list, 
                         usage="./doDiv.R infile [outfile]",
                         description=paste0("\tinfile: a merged metaphlan table\n",
                                            "\toutfile: a .tsv file. Default to stdout"))
  cl <- parse_args(Parser, positional_arguments=c(1,2))
  
  Table <- read.abundance.table(cl$args[1])
  default.taxlevels <- unlist(strsplit(cl$options$taxlevels, ""))
  Data <- split.table(Table, rm.strains=cl$options$rmstrains,
                      default.taxlevels=default.taxlevels)

  Data <- filter.table(Data)
  # diversity cannot calculate the distance when only one row, so setting to zero and add a dummyrow
  for (d in names(Data)){
    if(dim(Data[[d]])[1]<2){
      Data[[d]] = Data[[d]] * 0
      Data[[d]] <-rbind(Data[[d]], 0)
    }
  }
  diversities <- doDiv(Data, index=cl$options$index)

  if(is.null(dim(diversities))){
    diversities = t(as.data.frame(diversities))
    rownames(diversities) = colnames(Data[[1]])[1] # Get sample name
  } else{
    
  }

  out.div <- stdout()
  if( length(cl$args) == 2 ) out.div <- cl$args[2]
  my.write.table(diversities, file=out.div)
}
