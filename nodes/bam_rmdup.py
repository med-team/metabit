#!/usr/bin/env python

import os.path
from   pypeline.node import Node, NodeError
import tools.rmdup_collapsed as rmdup_collapsed


class FilterCollapsedBAMNode(Node):
    """Remove PCR duplicates from sorted bam file (collapsed reads only)"""

    def __init__(self, config, input_bams, output_bam, keep_dupes=False,
                 dependencies=()):
        
        if isinstance(input_bams, list) or isinstance(input_bams, tuple):
            if len(input_bams) > 1:
                raise NodeError("FilterCollapsedBAMNode cannot take multiple files in input")
            else:
                input_bam = input_bams[0]

        self.description = "<Remove duplicates from *collapsed* reads:\n" \
                           "  %s --> %s>" % (os.path.basename(input_bam),
                                             os.path.basename(output_bam))
        self.keep_dupes = keep_dupes
        Node.__init__(self, description=self.description,
                      input_files  = input_bam,
                      output_files = output_bam,
                      dependencies = dependencies)

    def _run(self, _config, _temp):
        """Overrides the _run function of the Node class"""
        
        args = [list(self.input_files)[0], list(self.output_files)[0]]
        
        if not self.keep_dupes:
            args.append("--remove-duplicates")
        
        return rmdup_collapsed.main(args)
        
