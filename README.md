# metaBIT pipeline #

An integrative and automated metagenomic pipeline for analysing microbial profiles from high-throughput sequencing shotgun data

## Brief description ##

The metaBIT pipeline proposes tools for visualising microbial profiles (barplots, heatmaps) and performing a range of statistical analyses (diversity indices, hierarchical clustering and principal coordinate analysis). It uses as input fastq files containing trimmed reads from shotgun high through-put sequencing.
metaBIT has been developed by Guillaume Louvel in the research group of Dr. Ludovic Orlando, at the Centre for GeoGenetics, Natural History Museum of Denmark, University of Copenhagen, Denmark.

For detailed instructions and an in-depth description of metaBIT, please refer to the companion Wiki site.

For questions, bug reports, and/or suggestions contact Guillaume Louvel (guillaume.louvel@ens.fr) or Kristian Hanghøj (k.hanghoej@snm.ku.dk)

## Citation ##

Guillaume Louvel, Clio Der Sarkissian, Kristian Hanghøj, Ludovic Orlando. *An integrative and automated metagenomic pipeline for analysing microbial profiles from high-throughput sequencing shotgun data*. Molecular Ecology Resources (2016)


***
[Go to the wiki](https://bitbucket.org/Glouvel/metabit/wiki/Home)
***
